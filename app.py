import asyncio
from collections import defaultdict
from copy import deepcopy
from datetime import datetime
import logging
import json
import os
import re
from urllib.parse import urlparse
import aiohttp
import asyncio_redis

from aiohttp import web
from aiohttp_ac_hipchat.addon import create_addon_app, require_jwt
from aiohttp_ac_hipchat.util import allow_cross_origin, http_request
import aiohttp_jinja2
import arrow
import bleach
import jinja2
import markdown


GLANCE_MODULE_KEY = "hcdecisions.glance"
TAG_PATTERN = re.compile('^#[a-zA-Z0-9_]+$')

log = logging.getLogger(__name__)

app = create_addon_app(plugin_key="hc-decisions",
                       addon_name="HC Decisions",
                       from_name="Decisions")

aiohttp_jinja2.setup(app, autoescape=True, loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'views')))


@asyncio.coroutine
def init_pub_sub():
    redis_url = app['config'].get('REDIS_URL')
    if not redis_url:
        redis_url = 'redis://localhost:6379'

    url = urlparse(redis_url)

    db = 0
    try:
        if url.path:
            db = int(url.path.replace('/', ''))
    except (AttributeError, ValueError):
        pass

    connection = yield from asyncio_redis.Connection.create(host=url.hostname, port=url.port, password=url.password, db=db)
    sub = yield from connection.start_subscribe()
    app["redis_sub"] = sub
    asyncio.async(reader(sub))

@asyncio.coroutine
def subscribe_new_client(client_id):
    chanel_key = "updates:{client_id}".format(client_id=client_id)
    log.debug("Subscribe to {0}".format(chanel_key))
    yield from app["redis_sub"].subscribe([chanel_key])

@asyncio.coroutine
def unsubscribe_client(client_id):
    chanel_key = "updates:{client_id}".format(client_id=client_id)
    log.debug("Unsubscribe to {0}".format(chanel_key))
    yield from app["redis_sub"].unsubscribe([chanel_key])

@asyncio.coroutine
def init(app):
    @asyncio.coroutine
    def send_welcome(event):
        client = event['client']
        yield from client.send_notification(app['addon'], text="HC Decisions was added to this room. Type "
                                                               "'/decision *vi* is better than emacs #editor' to get "
                                                               "started (yes, you can use Markdown).")

    app['addon'].register_event('install', send_welcome)
    yield from init_pub_sub()

app.add_hook("before_first_request", init)

@asyncio.coroutine
def capabilities(request):

    config = request.app["config"]
    base_url = config["BASE_URL"]
    response = web.Response(text=json.dumps({
        "links": {
            "self": base_url,
            "homepage": base_url
        },
        "key": config.get("PLUGIN_KEY"),
        "name": config.get("ADDON_NAME"),
        "description": "HipChat connect add-on that records decisions",
        "vendor": {
            "name": "Atlassian Labs",
            "url": "https://atlassian.com"
        },
        "capabilities": {
            "installable": {
                "allowGlobal": False,
                "allowRoom": True,
                "callbackUrl": base_url + "/installable"
            },
            "hipchatApiConsumer": {
                "scopes": [
                    "send_notification",
                    "view_room"
                ],
                "fromName": config.get("FROM_NAME")
            },
            "webhook": [
                {
                    "url": base_url + "/decision",
                    "event": "room_message",
                    "pattern": "^/decision(\s|$).*"
                }
            ],
            "glance": [
                {
                    "key": GLANCE_MODULE_KEY,
                    "name": {
                        "value": "Decisions"
                    },
                    "queryUrl": base_url + "/glance",
                    "target": "hcdecisions.sidebar",
                    "icon": {
                        "url": base_url + "/static/info.png",
                        "url@2x": base_url + "/static/info@2x.png"
                    }
                }
            ],
            "webPanel": [
                {
                    "key": "hcdecisions.sidebar",
                    "name": {
                        "value": "Decisions"
                    },
                    "location": "hipchat.sidebar.right",
                    "url": base_url + "/report"
                }
            ]
        }
    }))

    return response


@asyncio.coroutine
@require_jwt(app)
@allow_cross_origin
def get_glance(request):
    data = yield from glance_json(request.client)
    return web.Response(text=json.dumps(data))


@asyncio.coroutine
def glance_json(client):
    spec = get_decisions_spec(client)
    data = yield from decision_db(app).find(spec)

    tag_count = 0
    decision_count = 0
    for tag in data:
        tag_count += 1
        decision_count += len(tag.get('items', []))

    return {
        "label": {
            "type": "html",
            "value": "<strong>{}</strong> topics, <strong>{}</strong> decisions"
                .format(tag_count, decision_count)
        }
    }


@asyncio.coroutine
def decision_webhook(request):
    addon = request.app['addon']
    body = yield from request.json()
    client_id = body['oauth_client_id']
    client = yield from addon.load_client(client_id)

    command = str(body['item']["message"]["message"][len("/decision"):]).strip()
    from_user = body['item']['message']['from']
    room = body['item']['room']

    if not command:
        yield from display_help(app, client)
    elif command.startswith("#"):
        yield from display_tag_contents(app, client, tags=command)
    elif command.startswith("delete "):
        yield from delete_decisions(app, client, from_user, room, command.split(" ")[1:])
    elif command == "tags":
        yield from display_tags(app, client)
    else:
        yield from record_decision(app, client, request, room, from_user, command)

    return web.Response(status=204)

@asyncio.coroutine
def delete_decisions(app, client, from_user, room, tokens):
    if len(tokens) < 0 or len(tokens) > 2:
        yield from client.send_notification(app['addon'],
                                            text="Deletions take the form of: /decision TAG [DECISION_ID]")
        return

    tag = tokens[0]
    id = None if len(tokens) == 1 else tokens[1]
    if not TAG_PATTERN.match(tag):
        yield from client.send_notification(app['addon'],
                                            text="Invalid tag format")
        return

    if not id:
        yield from decision_db(app).remove(get_decisions_spec(client, tag))
        yield from client.send_notification(app['addon'], text="Deleted tag {}".format(tag))
    else:
        data = yield from decision_db(app).find_one(get_decisions_spec(client, tag))
        for item in data.get('items'):
            if item['id'] == int(id) - 1:
                item['deleted'] = True
                yield from decision_db(app).update(get_decisions_spec(client, tag), data)
                yield from client.send_notification(app['addon'], text="Deleted decision: {}".format(id))
                break
        else:
            yield from client.send_notification(app['addon'], text="Decision delete not found")

    yield from update_glance(app, client, room)
    yield from send_udpate(client)

@asyncio.coroutine
def display_tag_contents(app, client, tags):
    parsed = [x for x in tags.split(' ') if TAG_PATTERN.match(x)]
    if not parsed:
        yield from client.send_notification(app['addon'],
                                            text="No valid tags found.")
        return
    for tag in parsed:
        spec, decisions = yield from find_decisions(app, client, tag)

        if decisions:
            yield from client.send_notification(app['addon'], html=render_decisions(decisions))
        else:
            yield from client.send_notification(app['addon'], text="No decisions found for {}. "
                                                                   "Type '/decision vi is better than emacs #editor' "
                                                                   "to add a decision.".format(tag))

@asyncio.coroutine
def display_tags(app, client):
    spec = get_decisions_spec(client)
    data = yield from decision_db(app).find(spec)
    lines = []
    if data:
        for tag in data:
            items = tag.get('items', [])
            if items:
                last_msg_date = arrow.get(items[-1]['date']).humanize()
                lines.append("<b>{}</b> - {} decision{}, newest from {}".format(
                    tag['tag'],
                    len(tag['items']),
                    "s" if len(tag['items']) > 1 else "",
                    last_msg_date))
            else:
                lines.append("<b>{}</b> - No decisions".format(tag['tag']))

    if not lines:
        lines.append("No tags found")

    yield from client.send_notification(app['addon'], html="<br />".join(lines))


@asyncio.coroutine
def display_help(app, client):
    yield from client.send_notification(app['addon'], text="Available commands for /decision:\n"
                                                           "  Add a decision -- /decision A new decision #sometag\n"
                                                           "  List decisions -- /decision #sometag\n"
                                                           "  Delete decisions -- /decision delete #sometag [ID]\n"
                                                           "  List tags -- /decision tags")

@asyncio.coroutine
def record_decision(app, client, request, room, from_user, decision):
    tags = [t for t in decision.split(" ") if TAG_PATTERN.match(t)]
    if not tags:
        yield from client.send_notification(app['addon'],
                                            text="A tag is needed to record the decision against a topic.  Add them "
                                                 "in the form of #somename")
        return
    title = ' '.join([t for t in decision.split(" ") if not TAG_PATTERN.match(t)])
    for tag in tags:
        spec = get_decisions_spec(client, tag)

        data = yield from decision_db(app).find_one(spec)
        if not data:
            data = deepcopy(spec)

        items = data.get('items', [])
        items.append(dict(
            id=len(items),
            date=datetime.utcnow(),
            title=title,
            author=from_user
        ))
        data['items'] = items
        yield from decision_db(app).update(spec, data, upsert=True)

    yield from client.send_notification(app['addon'],
                                        text="Decision recorded.  Type '/decision {}' to see related decisions."
                                        .format(' '.join(tags)))
    yield from update_glance(app, client, room)
    yield from send_udpate(client)


@asyncio.coroutine
def update_glance(app, client, room):
    content = yield from glance_json(client)
    yield from push_glance_update(app, client, room['id'], {
        "glance": [{
                       "key": GLANCE_MODULE_KEY,
                       "content": content
                   }]
    })


@asyncio.coroutine
def push_glance_update(app, client, room_id_or_name, glance):
    token = yield from client.get_token(app['redis_pool'], scopes=['view_room'])
    with (yield from http_request('POST', "%s/addon/ui/room/%s" % (client.api_base_url, room_id_or_name),
                                  headers={'content-type': 'application/json',
                                           'authorization': 'Bearer %s' % token},
                                  data=json.dumps(glance),
                                  timeout=10)) as resp:
        if resp.status == 200:
            body = yield from resp.read(decode=True)
            return body['items']

@asyncio.coroutine
@require_jwt(app)
@aiohttp_jinja2.template('report.jinja2')
def report_view(request):
    """
        Render the report view
    """
    return {
        "base_url": app["config"]["BASE_URL"],
        "signed_request": request.signed_request,
        "room_id": request.jwt_data["context"]["room_id"]
    }


@asyncio.coroutine
@require_jwt(app)
def report_data(request):
    result = yield from get_report_data(request.client)
    return web.Response(body=json.dumps(result).encode('utf-8'), content_type="application/json")


@asyncio.coroutine
def get_report_data(client):
    spec = get_decisions_spec(client)
    data = yield from decision_db(app).find(spec)
    result = {}
    if data:
        for tag in data:
            result[tag['tag']] = []
            items = tag.get('items', [])
            for idx, item in enumerate(items):
                result[tag['tag']].append({
                    'title': render_decision(item),
                    'deleted': item.get('deleted', False),
                    'id': idx + 1
                })
    return result

@asyncio.coroutine
def keep_alive(websocket, ping_period=15):
    while True:
        yield from asyncio.sleep(ping_period)

        try:
            log.debug("Ping websocket")
            websocket.ping()
        except Exception as e:
            log.warn('Got exception when trying to keep connection alive, '
                     'giving up.')
            return

ws_connections = defaultdict(set)

@asyncio.coroutine
def reader(subscriber):
    while True:
        reply = yield from subscriber.next_published()
        yield from websocket_send_udpate(reply.value)

@asyncio.coroutine
def send_udpate(client):
    log.debug("Publish update to Redis".format(len(ws_connections)))
    chanel_key = "updates:{client_id}".format(client_id=client.id)
    yield from app['redis_pool'].publish(chanel_key, json.dumps({
        "client_id": client.id}))

@asyncio.coroutine
def websocket_send_udpate(json_data):
    data = json.loads(json_data)

    ws_connections_for_room = ws_connections[data["client_id"]]
    log.debug("Send update to {0} WebSocket".format(len(ws_connections_for_room)))
    for ws_connection in ws_connections_for_room:
        try:
            addon = app['addon']
            client = yield from addon.load_client(data['client_id'])
            result = yield from get_report_data(client)
            ws_connection.send_str(json.dumps(result))
        except RuntimeError as e:
            log.warn(e)
            ws_connections.remove(ws_connection)


@asyncio.coroutine
@require_jwt(app)
def websocket_handler(request):
    response = web.WebSocketResponse()
    ok, protocol = response.can_start(request)
    if not ok:
        return web.Response(text="Can't start webSocket connection.")

    response.start(request)

    asyncio.async(keep_alive(response))

    client_id = request.client.id

    ws_connections_for_room = ws_connections[client_id]
    if len(ws_connections_for_room) == 0:
        yield from subscribe_new_client(client_id)

    ws_connections_for_room.add(response)
    log.debug("WebSocket connection open ({0} in total)".format(len(ws_connections)))

    while True:
        try:
            msg = yield from response.receive()

            if msg.tp == aiohttp.MsgType.close:
                log.info("websocket connection closed")
            elif msg.tp == aiohttp.MsgType.error:
                log.warn("response connection closed with exception %s",
                         response.exception())
        except RuntimeError:
            break

    ws_connections_for_room = ws_connections.get(client_id)
    ws_connections_for_room.remove(response)

    if len(ws_connections_for_room) == 0:
        yield from unsubscribe_client(client_id)

    return response
#
# def status_to_view(status):
#     msg_date = arrow.get(status['date'])
#     message = status['message']
#     html = render_markdown_as_safe_html(message)
#
#     return {
#         "date": msg_date.humanize(),
#         "user": status['user'],
#         "message_html": html
#     }
    

def get_decisions_spec(client, tag=None):
    result = {
        "client_id": client.id,
        "group_id": client.group_id,
        "capabilities_url": client.capabilities_url
    }
    if tag:
        result['tag'] = tag

    return result


@asyncio.coroutine
def find_decisions(app, client, tag):
    spec = get_decisions_spec(client, tag)
    data = yield from decision_db(app).find_one(spec)
    result = []
    if data:
        result = data.get('items', [])

    return spec, result


def render_decisions(decisions):
    lines = []
    for idx, decision in enumerate(decisions):
        txt = render_decision(decision)
        lines.append("{idx}. {txt}".format(
            idx=idx + 1, txt=txt))
    return "<br />".join(lines)

def render_decision(decision):
    msg_date = arrow.get(decision['date'])
    message = decision['title']
    html = render_markdown_as_safe_html(message)
    html = html.replace("<p>", "")
    html = html.replace("</p>", "")
    name = decision['author']['name']
    return "{title} -- <i>{name}, {ago}</i>".format(
            title=html, name=name, ago=msg_date.humanize())

allowed_tags = bleach.ALLOWED_TAGS + ["p"]
def render_markdown_as_safe_html(message):
    html = markdown.markdown(message)

    return bleach.clean(html, tags=allowed_tags, strip=True)


def decision_db(app):
    return app['mongodb'].default_database['decisions']

app.router.add_static('/static', os.path.join(os.path.dirname(__file__), 'static'), name='static')
app.router.add_route('GET', '/', capabilities)
app.router.add_route('POST', '/decision', decision_webhook)
app.router.add_route('GET', '/glance', get_glance)
app.router.add_route('GET', '/report-data', report_data)
app.router.add_route('GET', '/report', report_view)
# app.router.add_route('GET', '/dialog', create_new_report_view)
# app.router.add_route('POST', '/create', create_new_report)
app.router.add_route('GET', '/websocket', websocket_handler)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(loop.create_server(
        app.make_handler(), '0.0.0.0', 5000))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

