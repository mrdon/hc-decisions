(function() {

  var tags = {};
  var selected_tag = '';

  function redraw() {
    if ($.isEmptyObject(tags)) {
      selected_tag = '';
    } else if (selected_tag == '') {
      selected_tag = Object.keys(tags)[0].substring(1);
    }

    $tags = $(".tags");
    $decisions = $(".decisions");
    $tags.empty();
    $decisions.empty();
    $.each(tags, function(key, value) {
      safe_tag = key.substring(1);
      $tags.append('<li class="tag_selector" data-tag="' + safe_tag + '" id="tag_selector_' + safe_tag + '">' +
                     '<a href="javascript:;">' + key + '</a></li>');
      $div = $('<ol class="decision_list hidden" id="decisions_' + safe_tag + '" reversed="reversed"/>');
      $(value.reverse()).each(function(index, value) {
        $li = $('<li/>');
        if (value.deleted) {
          $li.append("<del>" + value.title + "</del>");
        } else {
          $li.append(value.title);
        }
        $div.append($li);
      });
      $decisions.append($div);
    });
    $('#decisions_' + selected_tag).removeClass('hidden');
    $('#tag_selector_' + selected_tag).addClass('tag_selected');
  }

  function initWebSocket(baseUrl) {
    var signedRequest = $("meta[name=acpt]").attr("content");

    var uri = new URI(baseUrl);
    var socket = new WebSocket((uri.protocol() === "https" ? "wss://" : "ws://") +
            uri.hostname() +  (uri.port() ? (":" + uri.port()) : "") + "/websocket?signed_request=" + signedRequest);

    socket.onmessage = function(event) {

      tags = JSON.parse(event.data);
      redraw();
    };
  }

  $(document).ready(function() {

    var baseUrl = $("meta[name=base-url]").attr("content");

    var $spinner = $(".spinner-container");
    $spinner.spin("medium");

    initWebSocket(baseUrl);

    $.ajax({
      url: baseUrl + "/report-data",
      type: "GET",
      dataType: "json"
    }).done(function(data) {
      tags = data;
      $spinner.data().spinner.stop();
      redraw();
    });

    $('.tags').on('click', '.tag_selector', function() {
      $('.tag_selector').removeClass('tag_selected');
      $(this).addClass('tag_selected');
      tag = $(this).attr('data-tag');
      selected_tag = tag;
      $('.decision_list').addClass('hidden');
      $('#decisions_' + selected_tag).removeClass('hidden');
    })

  });



})();