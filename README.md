# HipChat Decisions

HipChat is great at asynchronous, informal communication to arrive at decisions quickly, but what if you miss that conversation?  
In a popular room, it can mean wading through pages of memes and lunch discussions to find 
the relevant bits to help you do your job.  

HipChat Decisions is a HipChat Connect add-on that helps you record decisions on any topic and easily share them with your team.

To get started, install via

https://atlassian.hipchat.com/addons/install?url=https://hc-decisions.herokuapp.com

To see a list of commands, type:

  /decision
